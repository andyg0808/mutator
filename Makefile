RUN ?= test
.PHONY: run
run:
	poetry run $(MAKE) test

.PHONY: test
test: mypy pytest clitest dirtest run_test

.PHONY: mypy
mypy:
	mypy --strict bin/run_test.py

.PHONY: pytest
pytest:
	pytest mutator_test.py

.PHONY: clitest
clitest:
	./cli_test

.PHONY: dirtest
dirtest:
	./dirtest

.PHONY: run_test
run_test:
	poetry run run_test --timeout=20 'pytest testcase.py' . testcase.py mutator_test.py mutator_test_example.py
	poetry run run_test 'true && pytest testcase.py' . testcase.py mutator_test.py mutator_test_example.py
	poetry run run_test 'pytest testcase.py' . testcase.py mutator_test.py mutator_test_example.py
	poetry run run_test 'pytest testcase.py' $(realpath .)/ testcase.py mutator_test.py mutator_test_example.py
	echo mutator_test.py | poetry run run_test 'pytest testcase.py' $(realpath .)/ testcase.py


.PHONY: watch
watch:
	git ls-files | poetry run entr -c test.sh $(MAKE) $(RUN)

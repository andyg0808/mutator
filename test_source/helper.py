from typing import List
import ast


def empty(stmt: ast.stmt) -> List[ast.stmt]:
    return [ast.copy_location(ast.Pass(), stmt)]

#!/usr/bin/env python3
import ast
import sys
from compiler import compiler
from dataclasses import dataclass
from difflib import unified_diff
from multiprocessing import Pool
from pathlib import Path
from subprocess import TimeoutExpired
from textwrap import indent

from docopt import docopt

import astor
from logbook import Logger, StderrHandler
from mutator import KilledMutation, Mutator
from runner import Runner

StderrHandler(level="NOTICE").push_application()
log = Logger("bin/mutator")

DOC = """
Usage:
    mutator [options] <command> <base> <file.py>

Copy <base> into temp directory, mutate <file.py> and save mutation as
<target.py> into temp copy, and run <command> in temp directory.

Options:
    --verbose  Print results of each failed test run
"""


@dataclass
class MutationResult:
    mutated: str


class CompiledMutantRunner(Runner[ast.Module]):
    def get_variant(self, variant: ast.Module, dest: Path) -> None:
        """Writes bytecode into a faked pyc file
        """
        with dest.open("w") as fi:
            fi.write(compiler(variant))


class MutantRunner(Runner[ast.Module]):
    def get_variant(self, variant: ast.Module, dest: Path) -> None:
        source_code = astor.to_source(variant)
        with dest.open("w") as fi:
            fi.write(source_code)


class MutationRunner:
    def __init__(self, command: str, base: str, target: Path, verbose: bool):
        self.target = target
        self.runner = MutantRunner(base, target.name, [command])
        self.runner.shell = True
        self.runner.verbose = verbose

    def run_mutation(self, mutant):
        if isinstance(mutant, KilledMutation):
            return mutant
        try:
            res = self.runner.run_test(mutant)
        except TimeoutExpired as e:
            return KilledMutation(f"duration", str(e))
        if res.returncode != 0:
            return KilledMutation(f"run {res.returncode}", str(res))
        return self.get_mutation_result(mutant, res)

    def get_mutation_result(self, mutant, res):
        return MutationResult(mutated=astor.to_source(mutant))

    def find_mutation_diff(self, target: Path, tree: ast.AST, result: MutationResult):
        source = astor.to_source(tree)
        diff = unified_diff(
            source.splitlines(),
            result.mutated.splitlines(),
            fromfile=target.name,
            tofile=target.name + " <mutant>",
        )
        return "\n".join(diff)


def load_tree(target: Path):
    with target.open() as fi:
        code = fi.read()
    return ast.parse(code)


def run_mutator(target: Path, runner: MutationRunner):
    tree = load_tree(target)
    m = Mutator(tree)
    p = Pool()
    results = list(p.map(runner.run_mutation, m))

    missed = 0
    killed = {}
    for mutant in results:
        if isinstance(mutant, KilledMutation):
            if mutant.stage in killed:
                killed[mutant.stage] += 1
            else:
                killed[mutant.stage] = 1
            continue
        else:
            diff = runner.find_mutation_diff(target, tree, mutant)
            missed += 1
        print("[38;5;51m======================================[0m")
        print("Missed mutant:")
        print(indent(diff, "   "))

    print("Killed mutants", killed)
    print("Total missed mutants:", missed)
    print("Total mutants", len(results))
    if missed > 0:
        sys.exit(2)


def launch():
    opts = docopt(DOC)

    target = Path(opts["<file.py>"])
    command = opts["<command>"]
    base = opts["<base>"]
    verbose = opts["--verbose"]

    runner = MutationRunner(command=command, base=base, target=target, verbose=verbose)

    run_mutator(target=target, runner=runner)

#!/usr/bin/env python3
import sys
from pathlib import Path
from shutil import copy
from typing import Iterable, List

from docopt import docopt  # type: ignore

from runner import Runner

DOCS = """
Usage:
    run_test [options] <program> <base> <target> [<variant>...]

If <variant> is not provided, run_test will read filenames from stdin.
Options:
    --verbose  Print output from commands
    --timeout=<time>  Time out command after <time> seconds [Default: 60]
"""


def main(
    program: str,
    base: str,
    target: str,
    variants: Iterable[str],
    verbose: bool,
    timeout: int,
) -> None:
    t = CopyRunner(base, target, [program])
    t.shell = True
    t.verbose = verbose
    t.timeout = timeout
    for variant in variants:
        print(f"Running {variant}", flush=True)
        res = t.run_test(Path(variant))
        if res.returncode != 0:
            print(res)
            sys.exit(2)
    print()


class CopyRunner(Runner[Path]):
    def get_variant(self, variant: Path, dest: Path) -> None:
        copy(variant, dest)


def launch() -> None:
    opts = docopt(DOCS)
    program = opts["<program>"]
    base = opts["<base>"]
    target = opts["<target>"]
    variants: Iterable[str] = opts["<variant>"]
    if not variants:
        variants = map(lambda x: x.rstrip(), sys.stdin)
    verbose = opts["--verbose"]
    timeout = int(opts["--timeout"])
    main(
        program=program,
        base=base,
        target=target,
        variants=variants,
        verbose=verbose,
        timeout=timeout,
    )
